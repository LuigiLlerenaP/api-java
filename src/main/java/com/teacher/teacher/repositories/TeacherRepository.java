package com.teacher.teacher.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.teacher.teacher.models.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {
    List<Teacher> findByLastName(String name);

    List<Teacher> findBySalaryLessThan(float salary);

    List<Teacher> findByAgeGreaterThan(Integer age);
}
