package com.teacher.teacher.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.teacher.teacher.models.Teacher;
import com.teacher.teacher.services.TeacherService;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@RequestMapping("/api/v1")
public class TeacherController {
    @Autowired
    private TeacherService teacherService;

    // !Get all teachers
    @GetMapping("/teachers")
    public List<Teacher> getAllTea() {
        return teacherService.getAllTeachers();
    }

    // !Get by id
    @GetMapping("/teachers/id/{id}")
    public Teacher findTeacherByIde(@PathVariable Long id) {
        return teacherService.findTeacherById(id);
    }

    // !Get by last name
    @GetMapping("/teachers/lastName/{lastName}")
    public List<Teacher> findTeacherByLastName(@PathVariable String lastName) {
        return teacherService.findTeachersByLastName(lastName);
    }

    // ! Get teacher whit age
    @GetMapping("/teachers/ageGreater")
    public List<Teacher> getTeachersWithAgeGreater() {
        return teacherService.getTeachersAgeGreater();
    }

    // !Get Teacher Salary
    @GetMapping("/teachers/salaryLess")
    public List<Teacher> getTeachersWithSalaryLess() {
        return teacherService.getTeachersSalaryLess();
    }

    // !Save
    @PostMapping("/teachers/save")
    public List<Teacher> saveTeacher(@RequestBody List<Teacher> teacher) {
        return teacherService.saveTeacher(teacher);
    }

    // !Update
    @PutMapping("/teachers/createUpdate")
    public List<Teacher> updateTeacher(@RequestBody List<Teacher> teacher) {
        return teacherService.updateOrCreateTeacher(teacher);
    }

    // ! Delete
    @DeleteMapping("/teachers/delete/{id}")
    public void deleteTeacher(@PathVariable Long id) {
        
        teacherService.deleteTeacher(id);
    }

    // !Delete all
    @DeleteMapping("/teachers/deleteAll")
    public void deleteAllTeachers() {
        teacherService.deleteAllTeachers();
    }

}
