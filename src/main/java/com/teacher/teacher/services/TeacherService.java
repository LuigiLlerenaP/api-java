package com.teacher.teacher.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teacher.teacher.models.Teacher;
import com.teacher.teacher.repositories.TeacherRepository;

import jakarta.persistence.EntityNotFoundException;

@Service
public class TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    // !Get all teachers
    public List<Teacher> getAllTeachers() {
        return teacherRepository.findAll();
    }

    // !Save teacher,
    public List<Teacher> saveTeacher(List<Teacher> teacher) {
        return teacherRepository.saveAll(teacher);
    }

    // ! Find teacher by ID
    public Teacher findTeacherById(Long id) {
        return teacherRepository.findById(id).orElse(null);
    }

    // ! Find teachers by last name
    public List<Teacher> findTeachersByLastName(String lastName) {
        return teacherRepository.findByLastName(lastName);
    }

    // ! Delete teacher by ID
    public void deleteTeacher(Long id) {
        teacherRepository.deleteById(id);
    }

    // ! Delete all
    public void deleteAllTeachers() {
        teacherRepository.deleteAll();
    }

    // ! Update teacher
    public List<Teacher> updateOrCreateTeacher(List<Teacher> teachers) {
        List<Teacher> updatedTeachers = new ArrayList<>();

        for (Teacher teacher : teachers) {
            Long id = teacher.getId();

            Teacher existingTeacher = (id != null && id > 0) ? teacherRepository.findById(id)
                    .orElseThrow(() -> new EntityNotFoundException("Teacher not found with id: " + id))
                    : null;

            if (existingTeacher != null) {
                existingTeacher.setFirstName(teacher.getFirstName());
                existingTeacher.setLastName(teacher.getLastName());
                existingTeacher.setAge(teacher.getAge());
                existingTeacher.setSalary(teacher.getSalary());

                updatedTeachers.add(teacherRepository.save(existingTeacher));
            }
            if (existingTeacher == null) {
                updatedTeachers.add(teacherRepository.save(teacher));
            }
        }

        return updatedTeachers;
    }

    // ! Get Age
    public List<Teacher> getTeachersAgeGreater() {
        return teacherRepository.findByAgeGreaterThan(35);
    }

    // ! Get Salary
    public List<Teacher> getTeachersSalaryLess() {
        return teacherRepository.findBySalaryLessThan(1500);
    }

}
